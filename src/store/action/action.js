export let fetchPage = (url) => (dispatch) => {
  fetch(url)
    .then((data) => data.json())
    .then((data) => dispatch({ type: "ALL_USER", payload: data }));
};

export let fetchPageById = (url) => (dispatch) => {
  fetch(url)
    .then((data) => data.json())
    .then((data) => {
      let data1 = { data: [data.data] };
      dispatch({ type: "SINGLE_USER", payload: data1 });
    });
};
