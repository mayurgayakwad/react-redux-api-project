import { combineReducers } from "redux";

const initial = {
  loading: false,
  data: null,
};

const PageData = (state = initial, action) => {
  switch (action.type) {
    case "ALL_USER":
      return { ...state, loading: false, data: action.payload };
    case "SINGLE_USER":
      return { ...state, loading: false, data: action.payload };
    default:
      return { ...state, loading: true };
  }
};

export default combineReducers({ PageData });
