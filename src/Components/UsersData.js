import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { fetchPage, fetchPageById } from "../store/action/action";
import Loader from "./Loader/loader";

class UsersData extends Component {
  userByPage = () => {
    let page = Math.floor(Math.random() * 3);
    console.log(page);
    this.props.fetchPage(`https://reqres.in/api/users?page=${page}`);
  };
  userByID = () => {
    let IDs = Math.floor(Math.random() * 10);
    console.log(IDs);
    this.props.fetchPageById(
      `https://reqres.in/api/users/${IDs == 0 ? 1 : IDs}`
    );
  };
  render() {
    let UserArray = this.props.data.PageData.data;

    // console.log(this.props.data.PageData.loading);
    console.log(UserArray);
    return (
      <>
        <div>
          <div className="ml-10 mt-10 mb-20">
            <Link to={"/"}>
              <button className="btn">Go To Home</button>
            </Link>
          </div>
          <center className="">
            <div className="mt-20">
              <div className="flex justify-center mt-10 gap-2">
                <button className="btn" onClick={this.userByPage}>
                  User by Page
                </button>
                <button className="btn" onClick={this.userByID}>
                  User by id
                </button>
              </div>
            </div>
          </center>
        </div>
        <div className="flex gap-10 justify-center mt-20  flex-wrap">
          {this.props.data.PageData.loading ? (
            <Loader />
          ) : (
            UserArray.data.map((info, index) => {
              return (
                <>
                  <div className="" key={`div${index}`}>
                    <h2 key={`h2${index}`} className="font-bold text-center">
                      {info.first_name}
                    </h2>
                    <h3 key={`h3${index}`}>{info.email}</h3>
                    <img
                      key={`img${index}`}
                      src={info.avatar}
                      className="ml-6 "
                      alt={info.avatar}
                    />
                  </div>
                </>
              );
            })
          )}
        </div>
      </>
    );
  }
}
function mapStateToProps(state) {
  return {
    data: state,
  };
}
export default connect(mapStateToProps, { fetchPage, fetchPageById })(
  UsersData
);
