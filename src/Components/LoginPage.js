import React, { Component } from "react";

import { Link } from "react-router-dom";

class LoginPage extends Component {
  render() {
    return (
      <div>
        <div className="ml-10 mt-10 mb-20">
          <Link to={"/"}>
            <button className="btn">Go To Home</button>
          </Link>
        </div>
        <center className="">
          <h2 className="text-2xl shadow-lg inline-block bg-white px-16 py-2 rounded-md">
            Create A User
          </h2>
          <div className=" mt-5 bg-white rounded-[20px] shadow-lg h-[300px] w-[400px] bg-wight ">
            <div className="flex flex-col pt-12  gap-10">
              <span className="flex justify-center gap-9">
                Email:{" "}
                <input
                  type="text"
                  className="border border-indigo-600 h-10 rounded-lg pl-4  placeholder:text-[15px]"
                  placeholder="Enter Your Name"
                />
              </span>
              <span className="flex justify-center gap-3 ">
                Password:{" "}
                <input
                  type="password"
                  className="border border-indigo-600 mb-8 h-10 rounded-lg pl-4 placeholder:text-[15px]"
                  placeholder="Enter Password"
                />
              </span>
            </div>
            <button type="submit" className="btn ">
              Create
            </button>
          </div>
        </center>
      </div>
    );
  }
}

export default LoginPage;
