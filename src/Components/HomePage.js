import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import "./HomePageStyle.css";
// import { useSelector } from "react-react";
class HomePage extends Component {
  render() {
    // const amount = useSelector((state) => state.amount);
    return (
      <div className="">
        <center>
          <div className="bg-[#6d30a4] min-h-full  text-white text-4xl py-4 ">
            Handling API with Redux
          </div>
          <div className="mt-40">
            <h2 className="text-2xl inline-block px-6 py-2 shadow-lg bg-slate-50">
              Select an option below
            </h2>
            <div className="flex justify-center mt-10 gap-2">
              <NavLink to={"/UsersData"}>
                {" "}
                <button className="btn">List User</button>
              </NavLink>
              <NavLink to={"/LoginPage"}>
                <button className="btn">Create User</button>
              </NavLink>
            </div>
          </div>
        </center>
      </div>
    );
  }
}

export default HomePage;
